import Vue from 'vue'
import Router from 'vue-router'
import InitialPage from '@/components/InitialPage'
import PanelCards from '@/components/PanelCards'
import About from '@/components/About'
import DetailCard from '@/components/DetailCard'
import ContactUs from '@/components/ContactUs'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'InitialPage',
      component: InitialPage
    },
    {
      path: '/noticias',
      name: 'news',
      component: PanelCards
    },
    {
      path: '/sobre',
      name: 'about',
      component: About
    },
    {
      path: '/palestras',
      name: 'lectures',
      component: PanelCards
    },
    {
      path: '/detalhes/:id',
      name: 'details',
      component: DetailCard
    },
    {
      path: '/fale-conosco',
      name: 'contactUs',
      component: ContactUs
    }
  ]
})
