// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/css/bootstrap.css'
import Axios from 'axios'
import { VueMaskDirective } from 'v-mask'

Vue.directive('mask', VueMaskDirective)
const API = 'http://bairo.pythonanywhere.com/'
// const API = 'http://192.168.0.128:8000/'

// Axios settings
Vue.prototype.$http = Axios.create({
  baseURL: API
})

Vue.use(BootstrapVue)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
